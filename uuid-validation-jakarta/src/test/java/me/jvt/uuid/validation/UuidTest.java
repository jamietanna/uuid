package me.jvt.uuid.validation;

import static org.assertj.core.api.Assertions.assertThat;

import jakarta.validation.*;
import jakarta.validation.executable.ExecutableValidator;
import java.lang.annotation.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class UuidTest {
  private final Validator jakartaValidator =
      Validation.buildDefaultValidatorFactory().getValidator();

  @Nested
  class AnnotationValidation extends ValidationTestCases {

    @Override
    Set<ConstraintViolation<Object>> process(String uuid) {
      MetaDataClass dataClass = new MetaDataClass();
      dataClass.uuid = uuid;

      return jakartaValidator.validate(dataClass);
    }

    private class MetaDataClass {

      @MetaAnnotation private String uuid;
    }
  }

  @Nested
  class FieldValidation extends ValidationTestCases {

    @Override
    Set<ConstraintViolation<Object>> process(String uuid) {
      DataClass data = new DataClass();
      data.uuid = uuid;

      return jakartaValidator.validate(data);
    }
  }

  @Nested
  class MethodArgumentValidation extends ValidationTestCases {

    @Override
    Set<ConstraintViolation<Object>> process(String uuid) {
      ExecutableValidator v = jakartaValidator.forExecutables();

      Method method;
      try {
        method = DataClass.class.getMethod("methodArgument", String.class);
      } catch (NoSuchMethodException e) {
        throw new IllegalStateException(e);
      }
      Object[] params = {uuid};
      return v.validateParameters(new DataClass(), method, params);
    }
  }

  @Nested
  class ConstructorParametersValidation extends ValidationTestCases {

    @Override
    Set<ConstraintViolation<Object>> process(String uuid) {
      ExecutableValidator v = jakartaValidator.forExecutables();

      Constructor<DataClass> constructor;
      try {
        constructor = DataClass.class.getConstructor(String.class);
      } catch (NoSuchMethodException e) {
        throw new IllegalStateException(e);
      }
      Object[] params = {uuid};
      return v.validateConstructorParameters(constructor, params);
    }
  }

  abstract class ValidationTestCases {
    abstract Set<ConstraintViolation<Object>> process(String uuid);

    @Test
    void doesNotContainViolationsWhenValid() {
      assertThat(process(UUID.randomUUID().toString())).isEmpty();
    }

    @Nested
    class WhenNull {
      private Set<ConstraintViolation<Object>> violations;

      @BeforeEach
      void setup() {
        violations = process(null);
      }

      @Test
      void containsViolation() {
        assertThat(violations).hasSize(1);
      }

      @Test
      void hasRelevantMessage() {

        assertThat(first(violations).getMessage())
            .isEqualTo(
                "must be a valid UUID, as defined by \"[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}\"");
      }
    }

    @Nested
    class WhenInvalid {
      private Set<ConstraintViolation<Object>> violations;

      @ParameterizedTest
      @ValueSource(strings = {"", "foo", "not valid"})
      void containsViolation(String value) {
        violations = process(value);

        assertThat(violations).hasSize(1);
      }

      @ParameterizedTest
      @ValueSource(strings = {"", "foo", "not valid"})
      void hasRelevantMessage(String value) {
        violations = process(value);

        assertThat(first(violations).getMessage())
            .isEqualTo(
                "must be a valid UUID, as defined by \"[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}\"");
      }
    }
  }

  @Nested
  class TypeUseValidation extends ValidationTestCases {

    @Override
    Set<ConstraintViolation<Object>> process(String uuid) {
      WithContainer obj = new WithContainer();
      obj.uuids = Collections.singletonList(uuid);
      return jakartaValidator.validate(obj);
    }

    private class WithContainer {
      private List<@Uuid String> uuids;
    }
  }

  private static ConstraintViolation<Object> first(Set<ConstraintViolation<Object>> set) {
    return set.iterator().next();
  }

  private static class DataClass {

    public DataClass() {}

    @SuppressWarnings("unused")
    public DataClass(@Uuid String uuid) {}

    @Uuid public String uuid;

    public void methodArgument(@Uuid String ignored) {}
  }

  @Uuid
  @Target({ElementType.ANNOTATION_TYPE, ElementType.PARAMETER, ElementType.FIELD})
  @Retention(RetentionPolicy.RUNTIME)
  @Constraint(validatedBy = {})
  @Documented
  private @interface MetaAnnotation {
    String message() default "There's something wrong in the metaverse";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
  }
}
