package me.jvt.uuid.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import jakarta.validation.ReportAsSingleViolation;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import java.lang.annotation.*;
import me.jvt.uuid.Patterns;

@NotNull
@Pattern(regexp = Patterns.UUID_STRING)
@Target({
  ElementType.TYPE_USE,
  ElementType.ANNOTATION_TYPE,
  ElementType.PARAMETER,
  ElementType.FIELD
})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {})
@Documented
@ReportAsSingleViolation
public @interface Uuid {
  String message() default "{me.jvt.uuid.validation.Uuid.message}";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}
