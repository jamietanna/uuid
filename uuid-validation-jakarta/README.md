# uuid-validation-jakarta

Java Bean validation using the `jakarta.validation` package, providing annotations which can be used to validate UUIDs are well-formed.
