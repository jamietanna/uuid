# uuid

A library to make it easier to work with and validate UUIDs in Java.

This is a multi-module project, which means this top-level project is not consumable. Please view one of the subdirectories, i.e. `uuid-core` for more information.

This library is released to Maven Central under the groupId `me.jvt.uuid`, and is licensed under the Apache 2.0 License.
