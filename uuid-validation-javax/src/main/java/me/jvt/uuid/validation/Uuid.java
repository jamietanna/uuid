package me.jvt.uuid.validation;

import java.lang.annotation.*;
import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import me.jvt.uuid.Patterns;

@NotNull
@Pattern(regexp = Patterns.UUID_STRING)
@Target({
  ElementType.TYPE_USE,
  ElementType.ANNOTATION_TYPE,
  ElementType.PARAMETER,
  ElementType.FIELD
})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {})
@Documented
@ReportAsSingleViolation
public @interface Uuid {
  String message() default "{me.jvt.uuid.validation.Uuid.message}";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}
