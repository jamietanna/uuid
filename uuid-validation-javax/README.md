# uuid-validation-javax

Java Bean validation using the `javax.validation` package, providing annotations which can be used to validate UUIDs are well-formed.
