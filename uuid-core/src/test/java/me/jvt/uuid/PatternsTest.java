package me.jvt.uuid;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.UUID;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

class PatternsTest {
  @Nested
  class Constructor {
    /*
    Because SonarQube needs higher code coverage
     */
    @Test
    void throwsNotSupportedException() {
      assertThatThrownBy(Patterns::new)
          .isInstanceOf(UnsupportedOperationException.class)
          .hasMessage("Utility class");
    }
  }

  @Nested
  class Uuid {
    @Nested
    class ByVersion {
      @Test
      void matchesWhenUuidVersion1() {
        assertThat(uuid("1")).matches(Patterns.UUID);
      }

      @Test
      void matchesWhenUuidVersion2() {
        assertThat(uuid("2")).matches(Patterns.UUID);
      }

      @Test
      void matchesWhenUuidVersion3() {
        assertThat(uuid("3")).matches(Patterns.UUID);
      }

      @Test
      void matchesWhenUuidVersion4() {
        assertThat(uuid("4")).matches(Patterns.UUID);
      }

      @Test
      void matchesWhenUuidVersion5() {
        assertThat(uuid("5")).matches(Patterns.UUID);
      }

      @Test
      void matchesWhenJavaUuid() {
        assertThat(UUID.randomUUID().toString()).matches(Patterns.UUID);
      }

      @Test
      void doesNotMatchNullUuid() {
        assertThat("00000000-0000-0000-0000-000000000000").doesNotMatch(Patterns.UUID_V4);
      }
    }

    @Nested
    class CaseSensitivity {
      @Test
      void matchesWhenLowercase() {
        assertThat(UUID.randomUUID().toString().toLowerCase()).matches(Patterns.UUID);
      }

      @Test
      void matchesWhenUppercase() {
        assertThat(UUID.randomUUID().toString().toUpperCase()).matches(Patterns.UUID);
      }
    }

    @ParameterizedTest
    @NullAndEmptySource
    void doesNotMatchNullOrEmpty(String uuid) {
      assertThat(uuid).doesNotMatch(Patterns.UUID);
    }

    @ParameterizedTest
    @ValueSource(strings = {"small", " ", "97c9543e-9f4b-45a6-b2cd-7c0253878e1d!"})
    void doesNotMatchNonUuidStrings(String value) {
      assertThat(value).doesNotMatch(Patterns.UUID);
    }
  }

  @Nested
  class UuidString {
    @Test
    void isValid() {
      assertThat(Patterns.UUID_STRING)
          .isEqualTo("[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}");
    }
  }

  @Nested
  class Uuidv4 {
    @Nested
    class ByVersion {
      @Test
      void doesNotMatchWhenUuidVersion1() {
        assertThat(uuid("1")).doesNotMatch(Patterns.UUID_V4);
      }

      @Test
      void doesNotMatchWhenUuidVersion2() {
        assertThat(uuid("2")).doesNotMatch(Patterns.UUID_V4);
      }

      @Test
      void doesNotMatchWhenUuidVersion3() {
        assertThat(uuid("3")).doesNotMatch(Patterns.UUID_V4);
      }

      @Test
      void matchesWhenUuidVersion4() {
        assertThat(uuid("4")).matches(Patterns.UUID_V4);
      }

      @Test
      void doesNotMatchWhenUuidVersion5() {
        assertThat(uuid("5")).doesNotMatch(Patterns.UUID_V4);
      }

      @Test
      void matchesWhenJavaUuid() {
        assertThat(UUID.randomUUID().toString()).matches(Patterns.UUID_V4);
      }

      @Test
      void doesNotMatchNullUuid() {
        assertThat("00000000-0000-0000-0000-000000000000").doesNotMatch(Patterns.UUID_V4);
      }
    }

    @Nested
    class CaseSensitivity {
      @Test
      void matchesWhenLowercase() {
        assertThat(UUID.randomUUID().toString().toLowerCase()).matches(Patterns.UUID_V4);
      }

      @Test
      void matchesWhenUppercase() {
        assertThat(UUID.randomUUID().toString().toUpperCase()).matches(Patterns.UUID_V4);
      }
    }

    @ParameterizedTest
    @NullAndEmptySource
    void doesNotMatchNullOrEmpty(String uuid) {
      assertThat(uuid).doesNotMatch(Patterns.UUID_V4);
    }

    @ParameterizedTest
    @ValueSource(strings = {"small", " ", "97c9543e-9f4b-45a6-b2cd-7c0253878e1d!"})
    void doesNotMatchNonUuidStrings(String value) {
      assertThat(value).doesNotMatch(Patterns.UUID_V4);
    }
  }

  @Nested
  class Uuidv4String {
    @Test
    void isValid() {
      assertThat(Patterns.UUID_V4_STRING)
          .isEqualTo(
              "[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[89abAB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}");
    }
  }

  private static String uuid(String version) {
    return String.format("123e4567-e89b-%s2d3-a456-426614174000", version);
  }
}
