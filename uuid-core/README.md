# `uuid-core`

A library exposing underlying utilities for UUID validation, such as `Pattern`s that describe common UUID formats.
